import React, { useEffect } from 'react';
import $ from 'jquery'; // Import jQuery
import './assets/css/all.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import { Dropdown } from 'bootstrap';

import './assets/css/font-style.css';
import Navbar from './components/Navbar';
import Hero from './components/Hero';
import About from './components/About';
import Project from './components/Project';
import FAQ from './components/FAQ';
import Contact from './components/Contact';

function App() {
    return (
        <div>
            <Navbar />
            <Hero />
            <About />
            <Project />
            <FAQ />
            <Contact />
        </div>
    );
}

export default App;

