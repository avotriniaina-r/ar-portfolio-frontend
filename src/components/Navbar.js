import React, { useState, useEffect } from 'react';
import Logo from "../assets/images/logo.png";

function Navbar() {
    const [scrolling, setScrolling] = useState(false);

    const handleScroll = () => {
        if (window.scrollY > 72) {
            setScrolling(true);
        } else {
            setScrolling(false);
        }
    };

    const handleNavLinkClick = (event, targetId) => {
        event.preventDefault();

        const targetElement = document.getElementById(targetId);
        if (targetElement) {
            window.scrollTo({
                top: targetElement.offsetTop - 49,
                behavior: 'smooth'
            });
        }
    };

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    return (
        <nav className={`navbar navbar-expand-lg ${scrolling ? 'scroll' : ''}`}>
            <div className="container">

                <a className="navbar-brand" href="/">
                    <img className="logo" src={Logo}/>
                </a>

                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav mx-auto">
                        <li className="nav-item">
                            <a href="#intro" className="nav-link smoothScroll" onClick={(e) => handleNavLinkClick(e, 'intro')}>Introduction</a>
                        </li>

                        <li className="nav-item">
                            <a href="#about" className="nav-link smoothScroll" onClick={(e) => handleNavLinkClick(e, 'about')}>About Me</a>
                        </li>

                        <li className="nav-item">
                            <a href="#projects" className="nav-link smoothScroll" onClick={(e) => handleNavLinkClick(e, 'projects')}>Projects</a>
                        </li>

                        <li className="nav-item">
                            <a href="#contact" className="nav-link smoothScroll" onClick={(e) => handleNavLinkClick(e, 'contact')}>Contact</a>
                        </li>
                    </ul>

                    <div className="mt-lg-0 mt-3 mb-4 mb-lg-0">
                        <a href="#" className="custom-btn btn" download>Download CV</a>
                    </div>
                </div>

            </div>
        </nav>
    );
}

export default Navbar;
