import React, { useState, useEffect } from 'react'; // Importez useState et useEffect

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';

function FAQ() {
    const [faqArticles, setFAQArticles] = useState([]); // État pour stocker les articles de la FAQ

    // Effectuer une requête à l'API pour récupérer les articles de la FAQ
    useEffect(() => {
        fetch('http://localhost:5000/faq') // Remplacez par l'URL appropriée pour accéder à l'API
            .then(response => response.json())
            .then(data => setFAQArticles(data))
            .catch(error => console.error('Erreur lors de la récupération des articles de FAQ:', error));
    }, []);

    const [activeCollapse, setActiveCollapse] = useState(null); // État pour suivre le collapse actif

    // Fonction pour gérer le clic sur les boutons de collapse
    const handleCollapse = (targetId) => {
        const target = document.getElementById(targetId);
        if (target) {
            const isExpanded = target.classList.contains('show');
            
            // Close all other accordions
            document.querySelectorAll('.collapse.show').forEach((accordion) => {
                if (accordion.id !== targetId) {
                    accordion.classList.remove('show');
                }
            });

            if (isExpanded) {
                target.classList.remove('show');
                setActiveCollapse(null);
            } else {
                target.classList.add('show');
                setActiveCollapse(targetId);
            }
        }
    };

    return (
        <section className="faq section-padding">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12 col-12">
                        <h3 className="mb-5">Frequently Asked Questions</h3>
                        <div className="accordion" id="accordion">
                            {faqArticles.map(article => (
                                <div className="card mb-1" key={article._id}>
                                    <div className="card-header" id={`heading-${article._id}`}>
                                        <h2 className="mb-0">
                                            <button
                                                className="btn btn-link"
                                                type="button"
                                                onClick={() => handleCollapse(`collapse-${article._id}`)}
                                                aria-expanded={activeCollapse === `collapse-${article._id}`}
                                            >
                                                {article.question}
                                            </button>
                                        </h2>
                                    </div>
                                    <div
                                        id={`collapse-${article._id}`}
                                        className="collapse"
                                        aria-labelledby={`heading-${article._id}`}
                                        data-parent="#accordion"
                                    >
                                        <div className="card-body">
                                            <p>{article.answer}</p>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                        <span className="faq-info-text">
                            Please send us a message if you have anything to say. Send an email message to{' '}
                            <strong>contact (at) tooplate (dot) com</strong>
                        </span>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default FAQ;
