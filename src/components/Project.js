import React from 'react';
import OwlCarousel from 'react-owl-carousel'; // Assurez-vous que vous importez correctement le composant
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/owl.carousel';
import ImgS from '../assets/images/projects/project-image01.jpg'


function Project() {
    const owlOptions = {
        loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            900: {
                items: 2,
            },
            1200: {
                items: 3,
                loop: false
            }
        }
    };

    return (
        <section className="projects section-padding" id="projects">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <h3 className="mb-5 text-center">Recents projects</h3>
                        <OwlCarousel className="owl-theme" {...owlOptions}>
                            <div className="item">
                                <div className="projects-thumb d-flex flex-column">
                                    <div className="projects-image">
                                        <img src={ImgS} className="img-fluid" alt="project 1"/>
                                    </div>
                                    <div className="projects-info">
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</p>
                                        <h6 className="mb-0">Ben</h6>
                                        <span>Art Director</span>
                                    </div>
                                </div>
                            </div>
                            <div className="item">
                                <div className="projects-thumb d-flex flex-column">
                                    <div className="projects-image">
                                        <img src={ImgS} className="img-fluid" alt="project 2"/>
                                    </div>
                                    <div className="projects-info">
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</p>
                                        <h6 className="mb-0">Marie</h6>
                                        <span>Marketing Consultant</span>
                                    </div>
                                </div>
                            </div>
                            <div className="item">
                                <div className="projects-thumb d-flex flex-column">
                                    <div className="projects-image">
                                        <img src={ImgS} className="img-fluid" alt="project 3"/>
                                    </div>
                                    <div className="projects-info">
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</p>
                                        <h6 className="mb-0">Jen</h6>
                                        <span>Product Manager</span>
                                    </div>
                                </div>
                            </div>
                            <div className="item">
                                <div className="projects-thumb d-flex flex-column">
                                    <div className="projects-image">
                                        <img src={ImgS} className="img-fluid" alt="project 4"/>
                                    </div>
                                    <div className="projects-info">
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</p>
                                        <h6 className="mb-0">Wilson</h6>
                                        <span>Web Developer</span>
                                    </div>
                                </div>
                            </div>
                        </OwlCarousel>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Project;

