import React, { useState } from 'react';
import axios from 'axios';
import toastr from 'toastr';
import 'toastr/build/toastr.min.css';

function Contact() {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');
  const [attachment, setAttachment] = useState(null);

  function handleSubmit(e) {
    e.preventDefault();

    const formData = new FormData();
    formData.append('name', name);
    formData.append('email', email);
    formData.append('message', message);
    if (attachment) {
      formData.append('attachment', attachment);
    }

    axios.post('http://localhost:5000/api/contact', formData)
      .then(response => {
        console.log(response.data.message);
        toastr.success('Message sent successfully!', 'Success', { positionClass: 'toast-center' });
        setName('');
        setEmail('');
        setMessage('');
        setAttachment(null);
      })
      .catch(error => {
        console.error('Error sending the form:', error);
        console.log('Server error response:', error.response);
        toastr.error('Error sending the form. Please try again later.', 'Error', { positionClass: 'toast-center' });
      });
  }
  return (
    <section className="contact section-padding pt-0" id="contact">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 col-md-6 col-12">
            <form className="contact-form webform" onSubmit={handleSubmit}>
              <div className="form-group d-flex flex-column-reverse">
                <input
                  type="text"
                  className="form-control"
                  name="cf-name"
                  id="cf-name"
                  placeholder="Your Name"
                  value={name}
                  onChange={e => setName(e.target.value)}
                required/>
                <label htmlFor="cf-name" className="webform-label">Full Name</label>
              </div>

              <div className="form-group d-flex flex-column-reverse">
                <input
                  type="email"
                  className="form-control"
                  name="cf-email"
                  id="cf-email"
                  placeholder="Your Email"
                  value={email}
                  onChange={e => setEmail(e.target.value)}
                required/>
                <label htmlFor="cf-email" className="webform-label">Your Email</label>
              </div>

              <div className="form-group d-flex flex-column-reverse">
                <textarea
                  className="form-control"
                  rows="5"
                  name="cf-message"
                  id="cf-message"
                  placeholder="Your Message"
                  value={message}
                  onChange={e => setMessage(e.target.value)}
                required></textarea>
                <label htmlFor="cf-message" className="webform-label">Message</label>
              </div>

              <div className="form-group d-flex flex-column">
                <label htmlFor="cf-attachment" className="mb-2">Attachment</label>
                <input
                  type="file"
                  className="form-control-file"
                  id="cf-attachment"
                  onChange={e => setAttachment(e.target.files[0])}
                />
              </div>

              <button type="submit" className="btn btn-primary" id="submit-button" name="submit">Send</button>
            </form>
          </div>
<div className="mx-auto col-lg-4 col-md-6 col-12">
                        <h3 className="my-4 pt-4 pt-lg-0">Avotriniaina R.</h3>

                        <p className="mb-1"><a href="tel:+261344101013">034 41 010 13</a> - <a href="tel:+261322273566">032 22 735 66</a></p>

                        <p>
                            <a href="mailto:contactavotriniaina@gmail.com">
                                contactavotriniaina@gmail.com
                                <i className="fas fa-arrow-right custom-icon"></i>
                            </a>
                        </p>

                        <ul className="social-links mt-2">
                            <li><a href="https://www.facebook.com/avotriniaina.dev" className="fab fa-facebook"></a></li>
                            <li><a href="https://github.com/ar-avotriniaina" className="fab fa-github"></a></li>
                            <li><a href="https://gitlab.com/ar-avotriniaina" className="fab fa-gitlab"></a></li>
                            <li><a href="https://www.linkedin.com/in/ar-avotriniaina/" className="fab fa-linkedin"></a></li>
                        </ul>

                        <p className="copyright-text mt-5 pt-3">Copyright &copy; <a href="/">Avotriniaina R.</a> 2023</p>
                    </div>
        </div>
      </div>
    </section>
  );
}

export default Contact;


