import React from 'react';
import Logo from "../assets/images/logo.png"
import trueAgency from "../assets/images/true-agency.jpg";

function About() {
    return (
        <section className="about section-padding" id="about">
            <div className="container">
                <div className="row">

                    <div className="col-lg-6 col-md-6 col-12">
                        <h3 className="mb-4"><img className="logo" alt="logo" src={Logo}/></h3>

                        <p>Depuis 2021, je m'épanouis en tant que développeur web full stack, jonglant habilement avec les technologies clés. Mon expertise s'étend sur l'écosystème Node.js pour la création d'API robustes, en parallèle avec React.js pour des interfaces frontales dynamiques. Mon savoir-faire englobe également des compétences pointues en PHP, HTML et CSS, renforcées par ma maîtrise des frameworks CSS tels que Bootstrap, qui optimisent mes créations.</p>

                        <p>En matière de bases de données, je navigue avec aisance à travers MongoDB et MySQL. Mon aptitude à structurer ces bases de données renforce la solidité de mes projets. Mon parcours témoigne d'une polyvalence essentielle dans le domaine du développement, une capacité à créer des solutions intégrées alliant technicité et créativité pour des expériences utilisateur exceptionnelles.</p>

                        <ul className="mt-4 mb-5 mb-lg-0 profile-list list-unstyled">
                            <li><strong>Nom Complet :</strong> Avotriniaina Razafindrakoto</li>
                            <li><strong>Date de Naissance :</strong> 10 Août 1993</li>
                            <li><strong>Portfolio :</strong> avotriniaina-r.github.io</li>
                            <li><strong>Email :</strong> contactavotriniaina@gmail.com</li>
                        </ul>
                    </div>

                    <div className="col-lg-5 mx-auto col-md-6 col-12">
                        <img src={trueAgency} className="about-image img-fluid" alt="Ben's Resume HTML Template" />
                    </div>

                </div>
                <div className="row about-third">
                    <div className="col-lg-4 col-md-4 col-12">
                        <h3>Développeur Web</h3>
                        <p>En tant que développeur web freelance, je crée des sites solides avec Node.js et React.js, maîtrisant PHP, HTML, CSS, et Bootstrap. J'utilise MongoDB et MySQL pour des projets polyvalents.</p>
                    </div>
                    <div className="col-lg-4 col-md-4 col-12">
                        <h3>Designer Web</h3>
                        <p>En tant que web designer, je conçois des interfaces esthétiques et intuitives, en harmonie avec les tendances actuelles. Mon travail s'intègre parfaitement avec le développement pour des expériences utilisateur exceptionnelles.</p>
                    </div>
                    <div className="col-lg-4 col-md-4 col-12">
                        <h3>Support IT</h3>
                        <p>En tant qu'IT Support freelance, je maintiens et sécurise les systèmes informatiques, offrant une assistance technique réactive. Je documente les processus et offre une formation pour une utilisation optimale des systèmes.</p>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default About;

