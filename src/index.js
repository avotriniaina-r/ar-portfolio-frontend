import React from 'react';
import { createRoot } from 'react-dom/client';
import $ from 'jquery';
import 'bootstrap/dist/css/bootstrap.min.css'; // Import du CSS Bootstrap
import 'bootstrap/dist/js/bootstrap.min.js';   // Import du JS Bootstrap

import App from './App';

const rootElement = document.getElementById('root');

const root = createRoot(rootElement);
root.render(<App />);

