# FRONT-END du Portfolio d'Avotriniaina Razafindrakoto

Ce portfolio présente Avotriniaina Razafindrakoto et offre des liens vers ses réalisations et projets.

## Table des matières

- [Introduction](#introduction)
- [Installation](#installation)
- [Utilisation](#utilisation)
- [Composants](#composants)
- [Styles](#styles)
- [Déploiement](#déploiement)
- [Contacts](#contacts)

## Introduction

Ce portfolio a été conçu pour présenter Avotriniaina Razafindrakoto, en offrant la possibilité de télécharger son CV. Il utilise React.js pour le frontend et communique avec le backend via des points d'API pour accéder aux données.

## Installation

1. Clonez ce dépôt : `git clone https://gitlab.com/ar-avotriniaina/ar-portfolio-frontend.git`
2. Accédez au répertoire du projet : `ar-portfolio-frontend`
3. Installez les dépendances : `npm install` ou `yarn install`

## Utilisation

Pour exécuter l'application en mode développement, utilisez la commande suivante :

```bash
npm start
```

L'application sera accessible à l'adresse `http://localhost:3000`.

## Composants

Le portfolio frontend est structuré autour de différents composants réutilisables. Voici une liste des principaux composants utilisés dans l'application :

- `Hero` : Affiche l'en-tête du portfolio avec une photo de moi.
- `About` : Présente des informations à propos de moi.
- `Faq` : Affiche une liste des questions les plus fréquemment posées concernant mes travaux.
- `ContactForm` : Affiche le formulaire de contact.

## Styles

Les styles de l'application sont gérés à l'aide de CSS et sont regroupés dans le répertoire `styles`. Les classes CSS sont appliquées aux composants pour styliser l'interface utilisateur.

## Déploiement

Pour déployer le portfolio en production, suivez ces étapes :

1. Configurez les variables d'environnement nécessaires pour le déploiement, telles que les URL d'API backend si nécessaire.
2. Exécutez la commande pour construire l'application : `npm run build` ou `yarn build`.
3. Déployez les fichiers générés dans le dossier `build` sur votre serveur ou plateforme de déploiement.

## Contacts

Pour toute question ou préoccupation, n'hésitez pas à contacter Avotriniaina Razafindrakoto à l'adresse suivante : [contactavotriniaina@gmail.com](mailto:contactavotriniaina@gmail.com).
